alias r := run
alias t := test
alias rr := run-release
alias f := fmt
alias l := lint
alias fl := fallible-lint
alias d := doc
alias b := build
alias uf := update-flake-dependencies
alias uc := update-cargo-dependencies
alias w := cargo-watch

_default:
    -@just --choose

test:
    cargo test -- --include-ignored

run:
    cargo run

fmt:
    cargo fmt
    nix develop .#fmtShell --command treefmt --config-file .treefmt.toml --tree-root ./.

build:
    cargo build

lint:
    nix run nixpkgs#typos
    cargo udeps
    cargo deny check
    cargo clippy

fallible-lint:
    -nix run nixpkgs#typos
    -cargo udeps
    -cargo deny check
    -cargo clippy

doc:
    cargo doc --open --offline --document-private-items

run-release:
    cargo run --release

update-flake-dependencies:
    nix flake update --commit-lock-file

# Update and then commit the `Cargo.lock` file
update-cargo-dependencies:
    cargo update
    git add Cargo.lock
    git commit Cargo.lock -m "update(cargo): \`Cargo.lock\`"

error:
    export RUST_LOG=error

info:
    export RUST_LOG=info

debug:
    export RUST_LOG=debug

cargo-watch:
    cargo watch -x check -x test -x build

cargo-public-api:
    nix develop .#lintShell --command cargo public-api

cargo-diff:
    nix develop .#lintShell --command cargo public-api diff
