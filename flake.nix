{
  description = "Manage GHub and GLab labels in the comfort of your own environment.";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-parts.url = "github:hercules-ci/flake-parts";
    rust-overlay.url = "github:oxalica/rust-overlay";
    rust-overlay.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = {
    self,
    flake-parts,
    ...
  } @ inputs:
    flake-parts.lib.mkFlake {inherit inputs;} {
      systems = [
        # "aarch64-linux"
        # "aarch64-darwin"
        # "x86_64-darwin"
        "x86_64-linux"
      ];
      perSystem = {
        # config,
        # self',
        # inputs',
        # pkgs,
        system,
        ...
      }: let
        overlays = [(import inputs.rust-overlay)];
        pkgs = import inputs.nixpkgs {inherit system overlays;};
        stdenv =
          if pkgs.stdenv.isLinux
          then pkgs.stdenvAdapters.useMoldLinker pkgs.stdenv
          else pkgs.stdenv;

        root = self;

        ignoreSource = [".git" "target" "example"];

        src = pkgs.nix-gitignore.gitignoreSource ignoreSource root;

        cargoToml = builtins.fromTOML (builtins.readFile (src + "/Cargo.toml"));
        inherit (cargoToml.package) version name;
        pname = name;
        rustToolchainToml = pkgs.rust-bin.fromRustupToolchainFile (src + "/rust-toolchain.toml");
        gitDate = self.lastModifiedDate;
        gitRev = self.shortRev or "Not committed yet.";

        postInstall = ''
          # generate and install the manpage
          export OUT_DIR=./.
          $out/bin/gabel setup --generate-manpage
          installManPage ./gabel.1

          # explicit behavior
          $out/bin/gabel setup --generate-completion bash > ./completions.bash
          installShellCompletion --bash --name ${pname}.bash ./completions.bash
          $out/bin/gabel setup --generate-completion fish > ./completions.fish
          installShellCompletion --fish --name ${pname}.fish ./completions.fish
          $out/bin/gabel setup --generate-completion zsh > ./completions.zsh
          installShellCompletion --zsh --name _${pname} ./completions.zsh
        '';

        cargoLock = {
          lockFile = builtins.path {
            path = src + "/Cargo.lock";
            name = "Cargo.lock";
          };
        };
        cargo = rustToolchainToml;
        rustc = rustToolchainToml;

        buildInputs = [
        ];
        nativeBuildInputs = [
          pkgs.installShellFiles
          pkgs.llvmPackages.bintools
        ];
        devInputs = [
          rustToolchainToml

          pkgs.rust-analyzer

          pkgs.just

          pkgs.cargo-flamegraph
          pkgs.cargo-watch
          pkgs.cargo-diet
          pkgs.cargo-nextest
          pkgs.cargo-tarpaulin

          (pkgs.symlinkJoin {
            name = "cargo-udeps-wrapped";
            paths = [pkgs.cargo-udeps];
            nativeBuildInputs = [pkgs.makeWrapper];
            postBuild = ''
              wrapProgram $out/bin/cargo-udeps \
                --prefix PATH : ${pkgs.lib.makeBinPath [
                (pkgs.rust-bin.selectLatestNightlyWith
                  (toolchain: toolchain.default))
              ]}
            '';
          })
        ];
        nightlyShellInputs = [
          (pkgs.rust-bin.selectLatestNightlyWith
            (toolchain: toolchain.default))
          pkgs.cargo-udeps
        ];
        fmtInputs = [
          pkgs.alejandra
          pkgs.treefmt
        ];
        ciInputs = [
          pkgs.typos
          pkgs.reuse
          pkgs.cargo-deny
        ];
        editorConfigInputs = [
          pkgs.editorconfig-checker
        ];

        meta = with pkgs.lib; {
          homepage = "https://github.com/a-kenji/gabel/";
          description = "Manage repo labels in the comfort of your own environment";
          license = [licenses.mit];
        };
      in rec {
        # Per-system attributes can be defined here. The self' and inputs'
        # module parameters provide easy access to attributes of the same
        # system.
        packages.default = (pkgs.makeRustPlatform {inherit cargo rustc;}).buildRustPackage {
          inherit
            buildInputs
            cargoLock
            meta
            nativeBuildInputs
            pname
            postInstall
            src
            stdenv
            version
            ;
          GIT_REV = gitRev;
          GIT_DATE = gitDate;
        };
        # nix run
        apps.default = {
          type = "app";
          program = "${packages.default}/bin/${name}";
        };

        devShells = {
          default = (pkgs.mkShell.override {inherit stdenv;}) {
            name = "gabel";
            inherit buildInputs;
            ### Environment Variables
            RUST_BACKTRACE = 1;
            nativeBuildInputs =
              nativeBuildInputs
              ++ devInputs
              ++ fmtInputs
              ++ ciInputs
              ++ editorConfigInputs;
            GIT_REV = gitRev;
            GIT_DATE = gitDate;
            GABEL_LOG = "debug";
          };
          fmtShell = pkgs.mkShell {
            name = "fmt-shell";
            nativeBuildInputs = fmtInputs;
          };
          nightlyShell =
            pkgs.mkShell
            {
              name = "nightly-shell";
              nativeBuildInputs = nightlyShellInputs;
            };
          ciShell = pkgs.mkShell {
            name = "ci-shell";
            nativeBuildInputs = ciInputs;
          };
          editorConfigShell = pkgs.mkShell {
            name = "ci-shell";
            nativeBuildInputs = editorConfigInputs;
          };
        };
        formatter = pkgs.alejandra;
      };
      flake = {
        inherit (inputs) nixpkgs;
        overlays = {
          default = _: prev: {
            inherit (self.packages.${prev.system}) gabel;
          };
          nightly = _: prev: {
            inherit (self.packages.${prev.system}) gabel;
          };
        };
      };
    };
}
