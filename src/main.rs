use crate::label::LabelDelimiter;
use std::fs;
use std::path::Path;

use crate::label::{parse_labels, Label};

use self::error::GabelError;
use clap::Parser;

mod cli;
mod error;
mod label;
mod log;
mod setup;

/// Manage GitHub and GitLab project labels from the comfort of your personal environment.
fn main() -> Result<(), GabelError> {
    log::init()?;
    let opts = cli::CliArgs::parse();
    tracing::debug!("Opened with opts: {:?}", opts);

    if let Some(command) = opts.get_command() {
        command.handle()?;
        return Ok(());
    };

    let labels = get_labels()?;
    let chosen_delimiter = opts.delimiter();
    let delimiter = labels.choose_delimiter(chosen_delimiter.as_ref());
    let delimiter = delimiter.map(|d| format!(" {} ", d));
    let output = show_output(&labels, &delimiter, &chosen_delimiter).unwrap();
    let t = temp_file::with_contents(output.as_bytes());
    open_editor(t.path(), &opts.editor()).unwrap();
    let contents = fs::read_to_string(t.path()).unwrap();
    let edited_labels = Label::from_editor(&contents, &delimiter).unwrap();
    // let _output = show_output(&edited_labels, &opts.delimiter()).unwrap();
    let diff = Label::diff(labels, edited_labels);

    if !diff.is_empty() {
        println!("{diff:#?}");
        if !opts.no_confirm() {
            if dialoguer::Confirm::new()
                .with_prompt("Do you want to apply the changes?")
                .interact()?
            {
                println!("Looks like you want to apply the changes.");
                for action in diff {
                    action.handle()?;
                }
                println!("Applied all the changes.");
            } else {
                println!("Looks like you don't want to apply the changes.");
            }
        }
    } else {
        println!("Nothing was changed.");
    }

    Ok(())
}

#[derive(Debug)]
enum ForgeCli {
    GitHub,
    GitLab,
}

/// Open a path with a specific editor.
/// Uses either the environment variable, or a fallback location.
fn open_editor(path: &Path, editor: &Option<&String>) -> Result<(), GabelError> {
    std::process::Command::new(editor.unwrap_or(&"nvim".to_owned()))
        .arg(path.as_os_str())
        .spawn()?
        .wait()?;
    Ok(())
}

/// Use the forge's cli in order to get the labels from the respective repository
fn get_labels() -> Result<Vec<Label>, GabelError> {
    let out = std::process::Command::new("gh")
        .arg("label")
        .arg("list")
        .output()?;

    if !&out.status.success() {
        let err_msg = std::str::from_utf8(&out.stderr).expect("Error message is not valid utf8.");
        return Err(GabelError::ForgeCli(err_msg.to_string()));
    }

    let utf8 = std::str::from_utf8(&out.stdout).unwrap();

    let parsed = parse_labels(utf8);
    Ok(parsed)
}

fn show_output(
    labels: &Vec<Label>,
    delimiter: &Option<String>,
    configured_delimiter: &Option<String>,
) -> Result<String, ()> {
    let mut out = String::new();
    let delimiter = delimiter.clone().unwrap_or_else(|| ",".to_string());
    let distance = '\t';

    let delimiter_msg = format!("# The currently chosen delimiter is: {}\n", delimiter);
    out.push_str(&delimiter_msg);

    if let Some(configured_delimiter) = configured_delimiter {
        let configured_delimiter_msg = format!(
            "# Warning: The configured delimiter {} was NOT used.\n",
            configured_delimiter
        );
        out.push_str(&configured_delimiter_msg);
    }

    for label in labels {
        out.push_str(label.name());
        out.push_str(&delimiter);
        out.push(distance);
        if let Some(description) = label.description() {
            out.push_str(description);
        }
        out.push_str(&delimiter);
        out.push(distance);
        if let Some(color) = label.color() {
            out.push_str(color);
        }
        out.push('\n');
    }
    Ok(out)
}
