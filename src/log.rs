use std::{fs, path::PathBuf, sync::Mutex};
use tracing::Level;
use tracing_subscriber::EnvFilter;
use tracing_subscriber::FmtSubscriber;

/// Configuration of logging
pub fn init() -> Result<(), std::io::Error> {
    let log_file = Some(PathBuf::from("/tmp/gabellog"));
    let log_file = match log_file {
        Some(path) => {
            if let Some(parent) = path.parent() {
                let _ = fs::create_dir_all(parent);
            }
            Some(fs::File::create(path)?)
        }
        None => None,
    };

    let env_filter = EnvFilter::builder()
        .with_env_var("GABEL_LOG")
        .parse("")
        .unwrap();

    let subscriber = FmtSubscriber::builder()
        .pretty()
        .with_timer(tracing_subscriber::fmt::time::UtcTime::new(
            time::macros::format_description!("[hour]:[minute]:[second].[subsecond digits:3]"),
        ))
        // all spans/events with a level higher than TRACE (e.g, debug, info, warn, etc.)
        // will be written to stdout.
        .with_max_level(Level::TRACE)
        .with_env_filter(env_filter)
        .with_writer(Mutex::new(log_file.unwrap()))
        .finish();

    tracing::subscriber::set_global_default(subscriber).expect("setting default subscriber failed");

    tracing::debug!("Initializing Logging");
    Ok(())
}
