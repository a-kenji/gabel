use thiserror::Error;

#[derive(Debug, Error)]
// The general Gabel error type
pub enum GabelError {
    /// Io Error
    #[error("IoError: {0}")]
    Io(#[from] std::io::Error),
    /// Io Error
    #[error("IoPathError: {0}")]
    IoPath(String),
    #[error("Utf8 Conversion Error: {0}")]
    Utf8(#[from] std::str::Utf8Error),
    #[error("No Label Name")]
    NoLabelName,
    #[error("No Completions for the following shell: {0}")]
    NoCompletionForShell(String),
    #[error("There was an error with the forge cli: {0}")]
    ForgeCli(String),
    #[error("There was an error generating the prompt: {0}")]
    DialoguerError(#[from] dialoguer::Error),
}
