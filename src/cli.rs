use std::str::FromStr;

use clap::{CommandFactory, Parser, Subcommand};
use clap_complete::Shell;

use crate::error::GabelError;

#[derive(Parser, Debug)]
#[command(author, version = CliArgs::unstable_version(), about, long_about = None)]
#[command(next_line_help = true)]
pub struct CliArgs {
    /// Force the specific delimiter to be used
    #[clap(short, long, value_parser, overrides_with = "delimiter")]
    delimiter: Option<String>,
    /// Whether to confirm potential changes before they will be applied
    #[clap(short, long, value_parser, overrides_with = "no_confirm")]
    no_confirm: bool,
    /// Specify the editor that should be used
    #[clap(short, long, value_parser, overrides_with = "editor", env = "EDITOR")]
    editor: Option<String>,
    /// Specify the forge-cli that should be preferred
    #[clap(short, long, value_parser, overrides_with = "forge", env = "FORGE")]
    forge: Option<String>,
    #[command(subcommand)]
    command: Option<Commands>,
}

#[derive(Debug, Subcommand)]
pub(crate) enum Commands {
    /// Functionality for setting up gabel
    #[clap(short_flag = 's')]
    Setup {
        /// Generates completion for the specified shell
        #[clap(long, value_name = "SHELL", value_parser)]
        generate_completion: Option<Shells>,
        /// Generates the manpage
        #[clap(long, value_parser)]
        generate_manpage: bool,
        /// Checks the configuration of gabel and displays
        /// the status of possibly dependent binaries
        #[clap(long, value_parser)]
        check: bool,
    },
}

impl Commands {
    /// Handles the [`Commands`] enum functionality
    pub(crate) fn handle(&self) -> Result<(), GabelError> {
        match &self {
            Commands::Setup {
                generate_completion,
                check,
                generate_manpage,
            } => {
                if *check {
                    let gh = std::process::Command::new("gh").arg("--version").output()?;
                    let glab = std::process::Command::new("glab")
                        .arg("--version")
                        .output()?;
                    let gh = std::str::from_utf8(&gh.stdout).unwrap();
                    let glab = std::str::from_utf8(&glab.stdout).unwrap();
                    println!("{gh}");
                    println!("{glab}");
                }

                if *generate_manpage {
                    let out_dir = std::env::var("OUT_DIR").expect("OUT_DIR is not set");
                    let out_path = std::path::PathBuf::from(out_dir)
                        .join(format!("{}.1", env!("CARGO_PKG_NAME")));
                    let app = CliArgs::command();
                    let man = clap_mangen::Man::new(app);
                    let mut buffer = Vec::<u8>::new();
                    man.render(&mut buffer)?;
                    std::fs::write(&out_path, buffer)?;
                    println!("Man page is generated at {:?}", out_path);
                }
                if let Some(shells) = generate_completion.clone() {
                    shells.generate_completion();
                }
            }
        }

        Ok(())
    }
}

#[derive(Debug, Clone)]
pub(crate) enum Shells {
    Bash,
    Fish,
    Zsh,
}

impl Shells {
    pub(crate) fn generate_completion(self) {
        let shell: Shell = Into::<Shell>::into(self);
        let mut out = std::io::stdout();
        clap_complete::generate(shell, &mut CliArgs::command(), "gabel", &mut out);
    }
}

impl From<Shells> for Shell {
    fn from(s: Shells) -> Self {
        match s {
            Shells::Bash => clap_complete::Shell::Bash,
            Shells::Fish => clap_complete::Shell::Fish,
            Shells::Zsh => clap_complete::Shell::Zsh,
        }
    }
}

impl FromStr for Shells {
    type Err = crate::error::GabelError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "Bash" | "bash" => Ok(Self::Bash),
            "Fish" | "fish" => Ok(Self::Fish),
            "Zsh" | "zsh" => Ok(Self::Zsh),
            e => Err(crate::error::GabelError::NoCompletionForShell(format!(
                "No completions for the following shell: {e}"
            ))),
        }
    }
}

impl CliArgs {
    /// Surface current version together with the current git revision and date, if available
    fn unstable_version() -> &'static str {
        const VERSION: &str = env!("CARGO_PKG_VERSION");
        let date = option_env!("GIT_DATE").unwrap_or("no_date");
        let rev = option_env!("GIT_REV").unwrap_or("no_rev");
        // This is a memory leak, only use sparingly.
        Box::leak(format!("{VERSION} - {date} - {rev}").into_boxed_str())
    }

    pub(crate) fn delimiter(&self) -> Option<String> {
        self.delimiter.clone()
    }

    pub(crate) fn editor(&self) -> Option<&String> {
        self.editor.as_ref()
    }

    /// To not shadow an external function
    pub(crate) fn get_command(&self) -> Option<&Commands> {
        self.command.as_ref()
    }

    pub fn no_confirm(&self) -> bool {
        self.no_confirm
    }
}
