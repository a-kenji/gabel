// Operations and types for the Label representation
use crate::error::GabelError;
use std::collections::{HashMap, HashSet};

const DEFAULT_DELIMITERS: [&str; 10] = [",", ";", "|", "@", "^", "[", "]", "{", "}", ""];

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub(crate) struct Label {
    name: String,
    description: Option<String>,
    color: Option<String>,
}

impl Label {
    pub(crate) fn name(&self) -> &str {
        self.name.as_ref()
    }

    pub(crate) fn description(&self) -> Option<&String> {
        self.description.as_ref()
    }

    pub(crate) fn color(&self) -> Option<&String> {
        self.color.as_ref()
    }
    /// Checks if the delimiter is already used in the [`Label`] itself,
    /// in that case a different delimiter should be used.
    ///
    /// Returns `true` if the delimiter is used in the label.
    pub(crate) fn probe_delimiter(&self, delimiter: &str) -> bool {
        if self.name().contains(delimiter) {
            return true;
        }
        if let Some(description) = self.description() {
            if description.contains(delimiter) {
                return true;
            }
        }
        false
    }
    /// Parse the labels back from the editor
    pub(crate) fn from_editor(
        s: &str,
        delimiter: &Option<String>,
    ) -> Result<Vec<Label>, GabelError> {
        let delimiter = delimiter.clone().unwrap_or_else(|| ",".to_string());
        let mut result = vec![];
        let raw_labels: Vec<&str> = s
            .trim()
            .split('\n')
            .filter(|line| !line.is_empty())
            .filter(|line| !line.starts_with('#'))
            .collect();
        for raw_label in raw_labels {
            let raw_label: Vec<&str> = raw_label.trim().splitn(3, &delimiter).collect();
            let name = if let Some(name) = raw_label.first() {
                if name.is_empty() {
                    return Err(GabelError::NoLabelName);
                } else {
                    name.to_string()
                }
            } else {
                return Err(GabelError::NoLabelName);
            };
            result.push(Label {
                name,
                description: raw_label.get(1).map(|s| s.trim()).map(ToString::to_string),
                color: raw_label.get(2).map(|s| s.trim()).map(ToString::to_string),
            });
        }
        Ok(result)
    }
    /// Show the difference between two label sets as a [`LabelAction`]
    /// Only if the label name stays the same, it is considered a Change.
    pub(crate) fn diff(first: Vec<Label>, second: Vec<Label>) -> Vec<LabelAction> {
        let first_set: HashSet<Label> = first.into_iter().collect();
        let second_set: HashSet<Label> = second.into_iter().collect();
        let remove_set: HashSet<_> = first_set.difference(&second_set).collect();
        let addition_set: HashSet<_> = second_set.difference(&first_set).collect();
        // let mut change_set: HashSet<_> = remove_set.intersection(&addition_set).collect();
        let mut additions = HashMap::new();
        let mut removals = HashMap::new();
        let mut result = vec![];

        // The removals and changes need to be checked by a primary key
        for addition in addition_set.iter() {
            additions.insert(addition.name(), <&Label>::clone(addition));
        }
        for removal in remove_set.iter() {
            removals.insert(removal.name(), <&Label>::clone(removal));
        }
        for key in additions.keys() {
            if let Some(removed_label) = removals.remove(key) {
                let add = &(*additions.get(key).unwrap()).clone();
                // change_set.insert(additions.get(key).unwrap());
                result.push(LabelAction::Change(removed_label.clone(), add.clone()));
            } else {
                let add = &(*additions.get(key).unwrap()).clone();
                result.push(LabelAction::Add(add.clone()));
            }
        }
        for key in removals.keys() {
            let remove = &(*removals.get(key).unwrap()).clone();
            result.push(LabelAction::Remove(remove.clone()));
        }
        result
    }
}

pub(crate) trait LabelDelimiter {
    /// Checks if the delimiter is already used in
    /// any of the [`Label`] themself,
    /// in that case a different delimiter should be used.
    ///
    /// Returns `true` if the delimiter is used in the label.
    fn probe_delimiter(&self, delimiter: &str) -> bool;
    /// Choose a delimiter that is not used in any of the labels,
    /// prefer to use a configured delimiter.
    fn choose_delimiter<S: AsRef<str> + std::fmt::Display>(
        &self,
        configured: Option<S>,
    ) -> Option<String>;
}

impl LabelDelimiter for Vec<Label> {
    fn probe_delimiter(&self, delimiter: &str) -> bool {
        self.iter().any(|label| label.probe_delimiter(delimiter))
    }

    fn choose_delimiter<S: AsRef<str> + std::fmt::Display>(
        &self,
        configured: Option<S>,
    ) -> Option<String> {
        if let Some(delimiter) = configured {
            if !self.probe_delimiter(delimiter.as_ref()) {
                return Some(delimiter.to_string());
            }
        }

        for delimiter in DEFAULT_DELIMITERS.iter() {
            if !self.probe_delimiter(delimiter) {
                return Some(delimiter.to_string());
            }
        }
        None
    }
}

/// Actions that can be performed on Labels
#[derive(Debug, PartialEq, PartialOrd, Eq, Ord)]
pub(crate) enum LabelAction {
    Remove(Label),
    Add(Label),
    Change(Label, Label),
}

impl LabelAction {
    pub(crate) fn handle(self) -> Result<(), GabelError> {
        match &self {
            LabelAction::Remove(label) => {
                let out = std::process::Command::new("gh")
                    .arg("label")
                    .arg("delete")
                    .arg(label.name())
                    .arg("--confirm")
                    .output()?;

                if !&out.status.success() {
                    let err_msg =
                        std::str::from_utf8(&out.stderr).expect("Error message is not valid utf8.");
                    return Err(GabelError::ForgeCli(err_msg.to_string()));
                }

                let utf8 = std::str::from_utf8(&out.stdout).unwrap();

                let _parsed = parse_labels(utf8);
            }
            LabelAction::Add(label) | LabelAction::Change(_, label) => {
                let mut cmd_builder = std::process::Command::new("gh");
                let cmd_builder = cmd_builder.arg("label").arg("create").arg(label.name());

                if let Some(description) = label.description() {
                    if !description.is_empty() {
                        cmd_builder.arg("--description");
                        cmd_builder.arg(description);
                    }
                }
                if let Some(color) = label.color() {
                    if !color.is_empty() {
                        cmd_builder.arg("--color");
                        cmd_builder.arg(color);
                    }
                }
                if let LabelAction::Change(_, _) = &self {
                    cmd_builder.arg("--force");
                }

                let out = cmd_builder.output()?;

                if !&out.status.success() {
                    let err_msg =
                        std::str::from_utf8(&out.stderr).expect("Error message is not valid utf8.");
                    return Err(GabelError::ForgeCli(err_msg.to_string()));
                }

                let utf8 = std::str::from_utf8(&out.stdout).unwrap();

                let _parsed = parse_labels(utf8);
            }
        }
        Ok(())
    }
}

impl std::str::FromStr for Label {
    type Err = GabelError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let raw_label: Vec<&str> = s.splitn(3, '\t').collect();
        let name = if let Some(name) = raw_label.first() {
            if name.is_empty() {
                return Err(GabelError::NoLabelName);
            } else {
                name.to_string()
            }
        } else {
            return Err(GabelError::NoLabelName);
        };
        Ok(Label {
            name,
            description: raw_label.get(1).map(|s| s.trim()).map(ToString::to_string),
            color: raw_label.get(2).map(|s| s.trim()).map(ToString::to_string),
        })
    }
}

pub(crate) fn parse_labels(out: &str) -> Vec<Label> {
    let raw_labels: Vec<&str> = out
        .trim()
        .split('\n')
        .filter(|line| !line.is_empty())
        .collect();
    let mut labels: Vec<Label> = vec![];
    for raw_label in raw_labels {
        if let Ok(label) = raw_label.parse() {
            labels.push(label)
        }
    }
    labels
}

#[cfg(test)]
mod tests {
    use super::*;
    fn gh_cli_default_output() -> &'static str {
        "bug\tSomething isn't working\t#d73a4a\ndocumentation\tImprovements or additions to documentation\t#0075ca\nduplicate\tThis issue or pull request already exists\t#cfd3d7\nenhancement\tNew feature or request\t#a2eeef\ngood first issue\tGood for newcomers\t#7057ff\nhelp wanted\tExtra attention is needed\t#008672\ninvalid\tThis doesn't seem right\t#e4e669\nquestion\tFurther information is requested\t#d876e3\nwontfix\tThis will not be worked on\t#ffffff\n"
    }
    fn gh_cli_output_single_line() -> &'static str {
        "bug\tSomething isn't working\t#d73a4a\n"
    }

    fn default_labels() -> &'static str {
        r#"
        bug,	Something isn't working,	#d73a4a
        documentation,	Improvements or additions to documentation,	#0075ca
        duplicate,	This issue or pull request already exists,	#cfd3d7
        enhancement,	New feature or request,	#a2eeef
        good first issue,	Good for newcomers,	#7057ff
        help wanted,	Extra attention is needed,	#008672
        invalid,	This doesn't seem right,	#e4e669
        question,	Further information is requested,	#d876e3
        wontfix,	This will not be worked on,	#ffffff
        "#
    }
    fn labels_two_deleted_labels() -> &'static str {
        r#"
        documentation,	Improvements or additions to documentation,	#0075ca
        duplicate,	This issue or pull request already exists,	#cfd3d7
        enhancement,	New feature or request,	#a2eeef
        good first issue,	Good for newcomers,	#7057ff
        help wanted,	Extra attention is needed,	#008672
        invalid,	This doesn't seem right,	#e4e669
        question,	Further information is requested,	#d876e3
        "#
    }
    fn labels_description_changed() -> &'static str {
        r#"
        bug,	Something is working,	#d73a4a
        documentation,	Improvements or additions to documentation,	#0075ca
        duplicate,	This issue or pull request already exists,	#cfd3d7
        enhancement,	New feature or request,	#a2eeef
        good first issue,	Good for newcomers,	#7057ff
        help wanted,	Extra attention is needed,	#008672
        invalid,	This doesn't seem right,	#e4e669
        question,	Further information is requested,	#d876e3
        wontfix,	This will not be worked on,	#ffffff
        "#
    }
    fn labels_name_changed() -> &'static str {
        r#"
        buggy,	Something isn't working,	#d73a4a
        documentation,	Improvements or additions to documentation,	#0075ca
        duplicate,	This issue or pull request already exists,	#cfd3d7
        enhancement,	New feature or request,	#a2eeef
        good first issue,	Good for newcomers,	#7057ff
        help wanted,	Extra attention is needed,	#008672
        invalid,	This doesn't seem right,	#e4e669
        question,	Further information is requested,	#d876e3
        wontfix,	This will not be worked on,	#ffffff
        "#
    }
    #[test]
    fn parse_default_label() {
        let output = gh_cli_default_output();
        let _labels = parse_labels(output);
    }
    #[test]
    fn parse_default_label_len() {
        let output = gh_cli_default_output();
        let labels = parse_labels(output);
        assert_eq!(9, labels.len());
    }
    #[test]
    fn parse_single_gh_cli_line() {
        let output = gh_cli_output_single_line();
        let label: Label = output.parse().unwrap();
        let label_goal = Label {
            name: "bug".into(),
            description: Some("Something isn't working".into()),
            color: Some("#d73a4a".into()),
        };
        assert_eq!(label, label_goal);
    }
    #[test]
    fn diff_equal() {
        let initial_labels = default_labels();
        let edited_labels = default_labels();
        let initial_labels = Label::from_editor(initial_labels, &None).unwrap();
        let edited_labels = Label::from_editor(edited_labels, &None).unwrap();
        let diff = Label::diff(initial_labels, edited_labels);
        assert_eq!(diff, vec![]);
    }
    #[test]
    fn cli_diff_equals_editor() {
        let initial_labels = gh_cli_default_output();
        let edited_labels = default_labels();
        let initial_labels = parse_labels(initial_labels);
        let edited_labels = Label::from_editor(edited_labels, &None).unwrap();
        let diff = Label::diff(initial_labels, edited_labels);
        assert_eq!(diff, vec![]);
    }
    #[test]
    fn diff_name_changed() {
        let initial_labels = default_labels();
        let edited_labels = labels_name_changed();
        let initial_labels = Label::from_editor(initial_labels, &None).unwrap();
        let edited_labels = Label::from_editor(edited_labels, &None).unwrap();
        let diff = Label::diff(initial_labels, edited_labels);
        let diff_goal = vec![
            LabelAction::Add(Label {
                name: "buggy".into(),
                description: Some("Something isn't working".into()),
                color: Some("#d73a4a".into()),
            }),
            LabelAction::Remove(Label {
                name: "bug".into(),
                description: Some("Something isn't working".into()),
                color: Some("#d73a4a".into()),
            }),
        ];
        assert_eq!(diff, diff_goal);
    }
    #[test]
    fn diff_description_changed() {
        let initial_labels = default_labels();
        let edited_labels = labels_description_changed();
        let initial_labels = Label::from_editor(initial_labels, &None).unwrap();
        let edited_labels = Label::from_editor(edited_labels, &None).unwrap();
        let diff = Label::diff(initial_labels, edited_labels);
        let diff_goal = vec![LabelAction::Change(
            Label {
                name: "bug".into(),
                description: Some("Something isn't working".into()),
                color: Some("#d73a4a".into()),
            },
            Label {
                name: "bug".into(),
                description: Some("Something is working".into()),
                color: Some("#d73a4a".into()),
            },
        )];
        assert_eq!(diff, diff_goal);
    }
    #[test]
    fn two_deleted_labels() {
        let initial_labels = default_labels();
        let edited_labels = labels_two_deleted_labels();
        let initial_labels = Label::from_editor(initial_labels, &None).unwrap();
        let edited_labels = Label::from_editor(edited_labels, &None).unwrap();
        let mut diff = Label::diff(initial_labels, edited_labels);
        let mut diff_goal = vec![
            LabelAction::Remove(Label {
                name: "bug".into(),
                description: Some("Something isn't working".into()),
                color: Some("#d73a4a".into()),
            }),
            LabelAction::Remove(Label {
                name: "wontfix".into(),
                description: Some("This will not be worked on".into()),
                color: Some("#ffffff".into()),
            }),
        ];
        diff_goal.sort();
        diff.sort();
        assert_eq!(diff, diff_goal);
    }
}
