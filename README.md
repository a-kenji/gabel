# gabel
[![Crates](https://img.shields.io/crates/v/gabel?style=flat-square)](https://crates.io/crates/gabel)
[![Built with Nix](https://img.shields.io/static/v1?label=built%20with&message=nix&color=5277C3&logo=nixos&style=flat-square&logoColor=ffffff)](https://builtwithnix.org)
[![Matrix Chat Room](https://img.shields.io/badge/chat-on%20matrix-1d7e64?logo=matrix&style=flat-square)](https://matrix.to/#/#gabel:matrix.org)

Manage GHub and GLab labels in the comfort of your own environment.

## Installation
```
cargo install --locked gabel
```
or 
```
nix flake install gitlab:a-kenji/gabel
```

## Run

```
nix run gitlab:a-kenji/gabel
```

## Features

- *Editor Integration*: Open the label configuration file in your favorite editor, making it straightforward to modify labels according to your preferences.
- *Diff Visualization*: Get a clear view of the changes made to your labels by displaying a diff between the previous and updated configurations.

## Usage:
- Run `gabel` to edit labels in your favorite editor.
- show a diff of the changes

## Dependencies
`gh` or `glab` cli binaries.
